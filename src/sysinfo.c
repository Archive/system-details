/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <string.h>
#include <errno.h>
#include <sys/systeminfo.h>		/* sysinfo(2) */

#include <netdb.h>			/* gethostbyname() */
#include <sys/types.h>			/* inet_ntoa() */
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <sys/stat.h>
#include <sys/swap.h>
#include <sys/loadavg.h>		/* getloadavg () */
#include <utmpx.h>			/* utmp */

#include <X11/Xlib.h>

#include <gdk/gdk.h>

#include <libxml/xmlmemory.h>
#include <libxml/parser.h>

#include "config.h"
#include "sysinfo.h"

const gchar *descriptions[] = {
	N_("Host Name:"),
	N_("Platform Type:"),
	N_("Internet (IP) Address:"),
	N_("Host ID:"),
	N_("Network Domain:"),
	N_("Internet Domain:"),
	N_("Physical Memory:"),
	N_("Virtual Memory (Swap):"),
	N_("Virtual Memory in Use:"),
	N_("Operating System:"),
	N_("Desktop:"),
	N_("System Status:"),
	N_("System Last Booted:"),
	N_("User Name:"),
	N_("X Display Name:"),
};

gchar *info[end_system_info];
static gchar SIbuffer[MAX_BUFFER];

typedef struct {
	gchar	*platform;
	gchar	*minor;
	gchar	*micro;
	gchar	*vendor;
	gchar	*date;
	gchar	*description;
} gnome_version;

/*
 * A wrapper around sysinfo () just for convenience sake
 */
void
SysInfo (gint command)
{
	glong ret;

	ret = sysinfo (command, SIbuffer, sizeof (SIbuffer));

	if (ret == -1)
		g_stpcpy (SIbuffer, "Unknown");
}

/*
 * Get the Workstation type 
 */
void
get_workstation_type ()
{
	SysInfo (SI_PLATFORM);
	info[si_type] = g_strdup (SIbuffer);
	
	SysInfo (SI_ARCHITECTURE);
	info[si_type] = g_strjoin (";", info[si_type], SIbuffer, NULL);
	
	SysInfo (SI_MACHINE);
	info[si_type] = g_strjoin (";", info[si_type], SIbuffer, NULL);
}

/*
 * Get the Host ID
 */
void
get_host_id ()
{
	glong hostid;

	SysInfo (SI_HW_SERIAL);
	hostid = atol (SIbuffer);
	info[si_hostid] = g_strdup_printf ("%x", hostid);
}

/*
 * IP Address
 */
static gchar *
get_ip_address (gchar *hostname)
{
	struct hostent *he;
	struct in_addr in;

	if ((he = gethostbyname (hostname)) == NULL)
		return "Unknown";

	(void) memcpy (&in, he->h_addr, he->h_length);

	return inet_ntoa (in);
}

/*
 * Get the hostname and generate the IP address
 */
void
get_ip_from_hostname ()
{
	SysInfo (SI_HOSTNAME);
	info[si_ip] = g_strdup (get_ip_address (SIbuffer));
}

/*
 * Get the Physical memory
 */
void
get_physical_memory ()
{
	long pagesize, pagecount;
	unsigned long pMem = 0;
	
	pagecount = sysconf (_SC_PHYS_PAGES);
	pagesize  = sysconf (_SC_PAGESIZE);

	/*
         * as of Solaris 2.4 up to 5GB of physical memory is supported.
         * immediately dividing pagecount by 1024 guarantees that
         * intermediate calculations will not overflow a long.
         * however it does cause the calculation to round down by 4Mb.
         * also on PCs, pagecount can be slightly off due to historic
         * architecture issues, so add 640K to prevent rounding errors.
         */
	if ((pagecount != -1) && (pagesize != -1)) {
		/* add 640K in case we are on a PC */
		if (pagesize > 0)
			pagecount += (0xA0000 / pagesize);
		/* prevent long overflow without rounding to nearest 4Mb */
		pMem = ((pagecount >> 6) * (pagesize >> 4)) >> 10;
	}
	info[si_mem] = g_strdup_printf ("%ld", pMem);
}

/*
 * Get the virtual memory
 */
static gboolean
get_virtual_memory (unsigned long *total, unsigned long *used)
{
	struct anoninfo ai;
	long pagesize;

	pagesize = sysconf (_SC_PAGESIZE);
	if (pagesize == -1) {
		return FALSE;
	}
	if (swapctl (SC_AINFO, &ai) == -1) {
		return FALSE;
	}

	*total  = ai.ani_max;
	*used   = ai.ani_resv;

	/*
	 * Convert to megabytes 
	 * Immediately dividing by 1024 gives us a fighting chance that
	 * intermediate calculations will not overflow an unsigned long.
	 *
	 * long double constants are used because there is no guarantee
	 * that total or used will be even multiples of 1024.
	 */

	*total  =  ((*total / 1024.0L) * pagesize) / 1024.0L;
	*used   =  ((*used  / 1024.0L) * pagesize) / 1024.0L;

	return TRUE;
}

gboolean
update_virtual_memory ()
{
	unsigned long vMem, vUsed;

	if (get_virtual_memory (&vMem, &vUsed)) {
		vUsed = (float) vUsed / (float) vMem * 100;
	} else {
		vUsed = 0;
	}
	info[si_mem_swap] = g_strdup_printf ("%ld", vMem);
	info[si_mem_inuse] = g_strdup_printf ("%ld%%", vUsed);
	return TRUE;
}

/*
 * Get Operating System release 
 */
void
get_os_release ()
{
	gchar *os = NULL, *rel = NULL, *vers = NULL;

	SysInfo (SI_SYSNAME);
	os = g_strdup (SIbuffer);

	SysInfo (SI_RELEASE);
	rel = g_strdup (SIbuffer);

	SysInfo (SI_VERSION);
	vers = g_strdup (SIbuffer);

	info[si_OS] = g_strdup_printf ("%s %s %s", os, rel, vers);
	
	g_free (os);
	g_free (rel);
	g_free (vers);

}

void
get_window_system_release ()
{
	gchar *vendor = NULL;
	gchar *version;
	gchar major, minor, micro;
	Display *d;

	d = XOpenDisplay (NULL);
	version = g_strdup_printf ("%d", VendorRelease (d));
	major = version[0];
	minor = version[1];
	micro = version[2];
	if (micro != 0) {
		version = g_strdup_printf ("%c.%c.%c", major, minor, micro);
	} else {
		version = g_strdup_printf ("%c.%c", major, minor);
	}
	vendor = ServerVendor (d);
	XCloseDisplay(d);
		
	info[si_desktop] = g_strdup_printf ("%s, X11 Version %s %s", 
					    info[si_desktop], vendor, version);
	g_free (version);
}

void
get_network_domain ()
{
	SysInfo (SI_SRPC_DOMAIN);
	info[si_ndomain] = g_strdup (SIbuffer);
}

/*
 * Get the Internet Domain
 */
void
get_internet_domain ()
{
	SysInfo (SI_SRPC_DOMAIN);
	info[si_idomain] = g_strdup (SIbuffer);
}

/*
 * get_hostname
 */
void
get_hostname ()
{
	SysInfo (SI_HOSTNAME);
	info[si_host] = g_strdup (SIbuffer);
}

/*
 * get user name
 */
void
get_user_and_display ()
{
	info[si_user] = g_strdup (g_get_user_name ());
	info[si_display] = g_strdup (gdk_get_display ());
}

/*
 * Get Gnome version and release
 */
void
get_gnome_vers_rel ()
{
	xmlDocPtr doc;
	gnome_version *ret;
	xmlNodePtr cur;
	gchar *vers = NULL, *rel = NULL;
	
	/*
	 * check whether ${prefix}/share/gnome-about/gnome-version.xml
	 * is present. Display the Gnome version and release only if 
	 * this file is present.
	 */
	if (access (PREFIX"/share/gnome-about/gnome-version.xml", F_OK) != 0) {
		return;
	}

	/*
	 * build an XML tree from a the file;
	 */
	doc = xmlParseFile (PREFIX"/share/gnome-about/gnome-version.xml");
	if (doc == NULL) return;

	/*
	 * Check the document is of the right kind
	 */
	cur = xmlDocGetRootElement (doc);
	if (cur == NULL) {
		xmlFreeDoc (doc);
		return;
	}

	/*
	 * Allocate the structure to be returned.
	 */
	ret = (gnome_version *) g_malloc (sizeof(gnome_version));
	if (ret == NULL) {
		xmlFreeDoc (doc);
		return;
	}
	memset (ret, 0, sizeof(gnome_version));

	/*
	 * Now, walk the tree.
	 */
	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp (cur->name, (const xmlChar *) "platform")) 
			ret->platform = (gchar *)
			xmlNodeListGetString (doc, cur->xmlChildrenNode, 1);
		if (!xmlStrcmp (cur->name, (const xmlChar *) "minor"))
			ret->minor = (gchar *)
			xmlNodeListGetString (doc, cur->xmlChildrenNode, 1);
		if (!xmlStrcmp (cur->name, (const xmlChar *) "micro"))
			ret->micro = (gchar *)
			xmlNodeListGetString (doc, cur->xmlChildrenNode, 1);
		if (!xmlStrcmp (cur->name, (const xmlChar *) "vendor"))
			ret->vendor = (gchar *)
			xmlNodeListGetString (doc, cur->xmlChildrenNode, 1);
		if (!xmlStrcmp (cur->name, (const xmlChar *) "date"))
			ret->date = (gchar *)
			xmlNodeListGetString (doc, cur->xmlChildrenNode, 1);
		if (!xmlStrcmp (cur->name, (const xmlChar *) "description"))
			ret->description = (gchar *)
			xmlNodeListGetString (doc, cur->xmlChildrenNode, 1);

		cur = cur->next;
	}

	if (ret->platform) {
		if (ret->minor) {
			if (ret->micro) 	
				vers = g_strjoin (".", ret->platform, 
						  ret->minor, ret->micro, NULL);
			else
				vers = g_strjoin (".", ret->platform, 
						  ret->minor, NULL);
		} else {
			vers = g_strdup (ret->platform);
		}
	}

	if (ret->vendor != NULL)
		rel = g_strdup (ret->vendor);
	if (ret->date != NULL)
		rel = g_strconcat (rel, " ", ret->date, NULL);
	if (ret->description != NULL)
		rel = g_strconcat (rel, " ", ret->description, NULL);

	if (vers && ret->description)
		info[si_desktop] = g_strdup_printf ("Gnome %s %s", vers, 
						     ret->description);
	else if (vers && !ret->description)
		info[si_desktop] = g_strdup_printf ("Gnome %s", vers);
	else if (!vers && ret->description)
		info[si_desktop] = g_strdup_printf ("Gnome %s", 
						     ret->description);
	else
		info[si_desktop] = NULL;

	g_free (vers);
	g_free (rel);
	g_free (ret);
	xmlFreeDoc (doc);
}

/*
 * Get uptime
 */
void
get_uptime ()
{
	struct utmp *utmpstruct;
	int numuser = 0;
	int ret;
	double loadavg[3];
	
	setutent();
	while ((utmpstruct = getutent())) {
		if ((utmpstruct->ut_type == USER_PROCESS) &&
		    (utmpstruct->ut_name[0] != '\0'))
			numuser++;
	}
	endutent();	
	if ((ret = getloadavg(loadavg, 3)) == -1)	{
		info[si_uptime] = g_strdup ("Unknown");
		return;
	}
	info[si_uptime] = g_strdup_printf ("%d users, load average: %.2f %.2f %.2f", 
					   numuser,
					   loadavg[LOADAVG_1MIN], 
					   loadavg[LOADAVG_5MIN], 
					   loadavg[LOADAVG_15MIN]);
		
}

/*
 * Get the last time the system was booted
 */
void
get_last_boot ()
{
	gchar buffer[MAX_BUFFER];
	struct utmpx *u, v;

	memset(buffer, '\0', MAX_BUFFER);
	v.ut_type = BOOT_TIME;
	u = getutxid(&v);	

	if (!u){
		info[si_last_boot] = g_strdup ("Unknown");
		return;
	}
	
	(void) cftime(buffer, "%A, %B %e, %Y %T", &(u->ut_tv.tv_sec));
	info[si_last_boot] = g_strdup (buffer);
}

/* 
 * Get the system information
 */
void
get_system_info ()
{
	get_workstation_type ();
	get_host_id ();	
	get_ip_from_hostname ();
	get_physical_memory ();
	update_virtual_memory ();
	get_os_release ();
	get_network_domain ();
	get_hostname ();
	get_user_and_display ();
	get_uptime ();
	get_internet_domain ();
	get_gnome_vers_rel ();
	get_window_system_release ();
	get_last_boot ();
}
