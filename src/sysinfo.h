/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef SYSINFO_H
#define SYSINFO_H

#include <stdio.h>
#include <stdlib.h>
#include <gnome.h>

#define RESPONSE_COPY 	-12 		/* Response for copy */
#define TIMEOUT		10000
#define MAX_BUFFER	257

/* The basic information to display in the main dialog. */
typedef enum {
	si_host,
	si_type,
	si_ip,
	si_hostid,
	si_ndomain,
	si_idomain,
	si_mem,          
	si_mem_swap,
	si_mem_inuse,
	si_OS,     
	si_desktop,
	si_uptime,
	si_last_boot,
	si_user,
	si_display,
	end_system_info
} system_info;

extern const gchar *descriptions[];
extern gchar *info[];

void get_system_info();
gboolean update_virtual_memory ();
void get_uptime ();
void get_network_domain ();
void get_internet_domain ();

#endif
