/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <sys/ddi.h>
#include <errno.h>
#include "config.h"
#include "sysinfo.h"

static gint update = -1;
static GtkWidget *app;
gchar *selected_string = NULL;
GtkWidget *scroll_window;
GtkWidget *copy_menu_item = NULL;
GtkTreeView *tree;

static void sysinfo_delete (GtkMenuItem *widget, gpointer data);
static void sysinfo_about (GtkWidget *widget, gpointer data);
void sysinfo_copy (GtkWidget *menuitem, gpointer *data);
void save_callback (GtkWidget *menuitem, gpointer data);

gboolean
sysinfo_update ();

/*
 * Menus
 */
static GnomeUIInfo file_menu[] = {
	GNOMEUIINFO_MENU_SAVE_AS_ITEM(save_callback, NULL),
	GNOMEUIINFO_MENU_QUIT_ITEM (sysinfo_delete, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo edit_menu[] = {
	GNOMEUIINFO_MENU_COPY_ITEM (sysinfo_copy, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo view_menu[] = {
	GNOMEUIINFO_ITEM_STOCK (N_("_Update Now"), N_("Update the fields"), 
				sysinfo_update, GTK_STOCK_REFRESH),
	GNOMEUIINFO_END
};

static GnomeUIInfo help_menu[] = {
	GNOMEUIINFO_HELP ("system-details"),
	GNOMEUIINFO_MENU_ABOUT_ITEM (sysinfo_about, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo sysinfo_menu[] = {
	GNOMEUIINFO_MENU_FILE_TREE (file_menu),
	GNOMEUIINFO_MENU_EDIT_TREE (edit_menu),
	GNOMEUIINFO_MENU_VIEW_TREE (view_menu),
	GNOMEUIINFO_MENU_HELP_TREE (help_menu),
	GNOMEUIINFO_END
};

static void
sysinfo_delete (GtkMenuItem *menuitem, gpointer data)
{
	gint i;

	for (i=0; i < end_system_info; i++) {
		if (info[i])
			g_free (info[i]);
	}
	gtk_timeout_remove (update);
	gtk_main_quit ();
}

static void
sysinfo_about (GtkWidget *widget, gpointer data)
{
	const gchar *authors[] = {
		"Deepa Chacko Pillai <deepa.chacko@wipro.com>",
		NULL
	};
	gchar *documenters[] = {
		NULL
	};					
	gchar *translator_credits = _("translator_credits");
	GtkWidget *about;

	about = gnome_about_new (_("System Details"), "1.0",
				 "Copyright 2002 by Deepa Chacko Pillai",
				 _("System Details\n."),
				 (const gchar **)authors,
				 (const gchar **)documenters,
				 strcmp (translator_credits, "translator_credits") !=0 ? translator_credits : NULL, 
				 NULL);
	gtk_widget_show(about);
}

gboolean
update_tree (GtkTreeView *view, const gchar *col1_items, 
	     gchar *col2_items) 
{
	GtkTreeIter iter;	
	GValue value = {0, };
	GtkTreeModel *model = gtk_tree_view_get_model (view);
	gchar *str;

	gtk_tree_model_get_iter_root (model, &iter);
	do {
		gtk_tree_model_get_value (model, &iter, 0, &value);
		str = g_strdup (g_value_get_string (&value));
		g_value_unset (&value);
		if (strcmp (str, col1_items) == 0) {
			gtk_list_store_set (GTK_LIST_STORE (model), &iter, 
					    1, col2_items, -1);
			g_free (str);
			return TRUE;
		}
		g_free (str);
	} while (gtk_tree_model_iter_next (model, &iter));
	return FALSE;
}

void 
fill_tree (GtkTreeView *view,
	   const gchar **col1_items, gchar **col2_items,
	   gint *width, gint *height)
{
	const gchar *titles[] = { N_("Category"), N_("Your System") };
	const gchar *row[2];
	gint i;
	gint col_zero_width, col_one_width;
	GtkRequisition req;
	GtkTreeModel *model = gtk_tree_view_get_model (view);
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeIter iter;
	PangoFontDescription *desc;
	PangoLayout *layout;
	PangoRectangle logical_rect;
	GdkRectangle rect;
	i = 0; col_zero_width = 0; col_one_width = 0;

	desc = pango_font_description_from_string ("Sans 12");
	layout = gtk_widget_create_pango_layout (GTK_WIDGET (view), "");
	pango_layout_set_font_description (layout, desc);
	pango_font_description_free (desc);

	while ( i < end_system_info ) {
		/*
		 * Don't display the row if either the description or info
		 * is NULL
		 */
		if ((col2_items[i] == NULL) || (col1_items[i] == NULL)) {
			++i;
			continue;
		}

		row[0] = _(col1_items[i]);
		row[1] = _(col2_items[i]);
		gtk_list_store_append ( GTK_LIST_STORE (model), &iter);
		gtk_list_store_set (GTK_LIST_STORE (model), &iter, 0, 
				    row[0], 1, row[1],-1);

		/*
		 * If the string is longer than any previous ones,
		 * increase the column width 
		 */
		pango_layout_set_text (layout, row[0], -1);	
		pango_layout_get_pixel_extents (layout, NULL, &logical_rect);
		col_zero_width = max (logical_rect.width, col_zero_width);

		pango_layout_set_text (layout, row[1], -1);	
		pango_layout_get_pixel_extents (layout, NULL, &logical_rect);
		col_one_width = max (logical_rect.width, col_one_width);

		++i;
	}
	g_object_unref (G_OBJECT (layout));

	renderer = gtk_cell_renderer_text_new ();

	/* Set the alignment to the right */
	renderer->xalign = 1;

	/* First Column */
	column = gtk_tree_view_column_new_with_attributes (titles[0],
							   renderer,
							   "text", 0,
							   NULL);
	gtk_tree_view_column_set_sizing (column, GTK_TREE_VIEW_COLUMN_AUTOSIZE);
	gtk_tree_view_column_set_fixed_width (column, col_zero_width + 10);
	gtk_tree_view_append_column (view, column);

	/* Second column */
	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (titles[1],
							   renderer,
							   "text",
							   1,
							   NULL);
	gtk_tree_view_column_set_sizing (column, GTK_TREE_VIEW_COLUMN_AUTOSIZE);
	gtk_tree_view_column_set_fixed_width (column, col_one_width);
	gtk_tree_view_append_column (view, column);

	*width = col_zero_width+col_one_width + 30;
	gtk_widget_size_request (GTK_WIDGET (view), &req);
	*height = req.height + 30;
}

GtkTreeView *
create_tree()
{
	GtkTreeView *view;
	GtkListStore *store;
	GtkTreeSelection *selection;

	store = gtk_list_store_new (2,
				    G_TYPE_STRING,
				    G_TYPE_STRING);

	view = GTK_TREE_VIEW (gtk_tree_view_new_with_model (GTK_TREE_MODEL(store)));
	g_object_unref (G_OBJECT (store));

	selection = gtk_tree_view_get_selection (view);
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_MULTIPLE);

	return view;
}

static void 
write_to_filestream (FILE * f)
{
	gint i = 0;
	while ( i < end_system_info ) {
		if (info[i] == NULL) {
			/* No information on this. */
			;
		}
		else {
			fprintf (f, "%-30s %s\n", _(descriptions[i]), info[i]);
		}
		++i;
	}
}

static gchar * 
write_to_string ()
{
	gchar* final = NULL;
	gchar* tmp  = NULL;
	gint i = 0;
	while ( i < end_system_info ) {
		if (info[i] == NULL) {
			/* No information on this. */
			;
		} else {
			gchar buf[MAX_BUFFER];
			g_snprintf (buf, MAX_BUFFER, "%-30s %s\n", 
				    _(descriptions[i]), info[i]);
			tmp = g_strconcat(final ? final : "", buf, NULL);
			g_free(final);
			final = tmp;
		}
		++i;
	} 
	return final;
}

 /* Creating a button with the given text and stock id 
  * code taken from gedit 
  */

static GtkWidget*
system_button_new_with_stock_image (const gchar* text, const gchar* stock_id)
{
	GtkWidget *button;
	GtkStockItem item;
	GtkWidget *label;
	GtkWidget *image;
	GtkWidget *hbox;
	GtkWidget *align;

	button = gtk_button_new ();

	if (GTK_BIN (button)->child)
		gtk_container_remove (GTK_CONTAINER (button),
				      GTK_BIN (button)->child);

	if (gtk_stock_lookup (stock_id, &item)) {
		label = gtk_label_new_with_mnemonic (text);
		gtk_label_set_mnemonic_widget (GTK_LABEL (label), GTK_WIDGET (button));

		image = gtk_image_new_from_stock (stock_id, GTK_ICON_SIZE_BUTTON);
		hbox = gtk_hbox_new (FALSE, 2);

		align = gtk_alignment_new (0.5, 0.5, 0.0, 0.0);

		gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);
		gtk_box_pack_end (GTK_BOX (hbox), label, FALSE, FALSE, 0);
		gtk_container_add (GTK_CONTAINER (button), align);
		gtk_container_add (GTK_CONTAINER (align), hbox);
		gtk_widget_show_all (align);

		return button;
	}

	label = gtk_label_new_with_mnemonic (text);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), GTK_WIDGET (button));

	gtk_misc_set_alignment (GTK_MISC (label), 0.5, 0.5);

	gtk_widget_show (label);

	gtk_container_add (GTK_CONTAINER (button), label);

	return button;
}

static GtkWidget*
system_dialog_add_button (GtkDialog *dialog, const gchar* text, const gchar* stock_id,
                         gint response_id)
{
	GtkWidget *button;

	g_return_val_if_fail (GTK_IS_DIALOG (dialog), NULL);
	g_return_val_if_fail (text != NULL, NULL);
	g_return_val_if_fail (stock_id != NULL, NULL);

	button = system_button_new_with_stock_image (text, stock_id);
	g_return_val_if_fail (button != NULL, NULL);

	GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);

	gtk_widget_show (button);

	gtk_dialog_add_action_widget (dialog, button, response_id);

	return button;
}


static void 
file_selection_cb (GtkWidget *button, gpointer fs)
{
	const gchar *file_name;
	FILE *fp;

	file_name = gtk_file_selection_get_filename (GTK_FILE_SELECTION(fs));
	if (!strlen (file_name))
		return;

	/* Check if the file already exists */
	if (g_file_test (file_name, G_FILE_TEST_EXISTS)) {
		GtkWidget* dialog;

		/* Check if it is a folder */
		if (g_file_test (file_name, G_FILE_TEST_IS_DIR)) {
			dialog = gtk_message_dialog_new (NULL, GTK_DIALOG_MODAL,
							 GTK_MESSAGE_ERROR,
							 GTK_BUTTONS_OK,
							 _("'%s' is a folder.\nPlease enter a filename."),
							 file_name);
	
			g_signal_connect (G_OBJECT (dialog), "response",
					  G_CALLBACK (gtk_widget_destroy), NULL);
			gtk_widget_show (dialog);
			return;
		}

		dialog = gtk_message_dialog_new (NULL, 0,
						 GTK_MESSAGE_WARNING,
						 GTK_BUTTONS_NONE,
						 _("File '%s' already exists.\nDo you want to overwrite it?"),
						 file_name);

		system_dialog_add_button (GTK_DIALOG (dialog),
					  _("Cancel"),
					  GTK_STOCK_CANCEL,
					  GTK_RESPONSE_CANCEL);

		system_dialog_add_button (GTK_DIALOG (dialog),
					  _("Overwrite"), 
					  GTK_STOCK_REFRESH,
					  GTK_RESPONSE_YES);


		switch (gtk_dialog_run (GTK_DIALOG (dialog))) {
			case GTK_RESPONSE_CANCEL:
			gtk_widget_destroy (dialog);
			return;

			default:
			gtk_widget_destroy (dialog);
			break;
		}
	}


	/* Check if the file can be written */

	fp = fopen (file_name, "w");
	if ( errno == EACCES ) {
		GtkWidget* dialog;
		dialog = gtk_message_dialog_new (NULL, GTK_DIALOG_MODAL,
						 GTK_MESSAGE_ERROR,
						 GTK_BUTTONS_OK,
						 _("Access denied to write to the file '%s'.\nPlease enter another filename."),
						 file_name);
	
		g_signal_connect (G_OBJECT (dialog), "response",
				  G_CALLBACK (gtk_widget_destroy), NULL);
		gtk_widget_show (dialog);
		return;
	}

	if (fp) {
		gtk_widget_hide (GTK_WIDGET (fs));
		write_to_filestream (fp);

		if ( fclose (fp) != 0 ) {
			GtkWidget* dialog;
			gchar *err_msg = g_strdup_printf (_("Error closing file `%s': \n"
					   	          "%s\n"
					    	          "Some or all data may not have been written."),
					    	          file_name,
					    	          g_strerror (errno));
			dialog = gtk_message_dialog_new (NULL,
							 GTK_DIALOG_DESTROY_WITH_PARENT,
							 GTK_MESSAGE_ERROR,
							 GTK_BUTTONS_CLOSE,
							 err_msg);
			g_signal_connect (dialog, "response",
					  G_CALLBACK (gtk_widget_destroy),
					  NULL);
			g_free (err_msg);
		}
		} else {
			GtkWidget* dialog;
			gchar *err_msg = g_strdup_printf (_("Couldn't open file `%s': %s"),
						          file_name,
						          g_strerror (errno));
			dialog = gtk_message_dialog_new (NULL,
							 GTK_DIALOG_DESTROY_WITH_PARENT,
							 GTK_MESSAGE_ERROR,
							 GTK_BUTTONS_CLOSE,
							 err_msg);
			g_signal_connect (dialog, "response",
					  G_CALLBACK (gtk_widget_destroy),
					  NULL);
			g_free (err_msg);
		}
		gtk_widget_destroy (GTK_WIDGET (fs));
}

static void 
save_callback (GtkWidget *menuitem, gpointer data)
{
	static GtkWidget *fs = NULL;

	if (fs != NULL)
	{
		gdk_window_show (fs->window);
		gdk_window_raise (fs->window);
		return;
	}

	fs = gtk_file_selection_new (_("Save System Information As..."));
	gnome_window_icon_set_from_default (GTK_WINDOW (fs));
	g_signal_connect (GTK_FILE_SELECTION (fs)->ok_button, "clicked",
		          G_CALLBACK (file_selection_cb), fs);

	g_signal_connect_swapped (GTK_FILE_SELECTION (fs)->cancel_button, 
				  "clicked",
				  G_CALLBACK (gtk_widget_destroy), 
				  G_OBJECT (fs));
	/*
	 * Mark the dialog as destroyed when it is destroyed so 
	 * we don't try to show it again 
	 */
	g_signal_connect (fs, "destroy",
			  G_CALLBACK (gtk_widget_destroyed), &fs);
 
	gtk_widget_show (fs);
}

static void
get_selected_string (GtkTreeModel *model, GtkTreePath *path_buf,
		     GtkTreeIter *iter, GtkTreeView *cl)
{
        GValue value = {0, };
	gchar *str;
	gchar *str1;
	gchar *tmp;
	gchar *tmp_sel;

	/* Get the column zero string */
        gtk_tree_model_get_value (model, iter, 0, &value);
        str = g_strdup (g_value_get_string (&value));
        g_value_unset (&value);

	/* Get the column one string */
        gtk_tree_model_get_value (model, iter, 1, &value);
        str1 = g_strdup (g_value_get_string (&value));
	tmp = g_strjoin (" ", str, str1, NULL);
	if (selected_string) {
		tmp_sel = selected_string;
		selected_string = g_strjoin ("\n", tmp_sel, tmp, NULL);
		g_free (tmp_sel);
	} else {
		selected_string = g_strdup (tmp);
	}
	g_free (tmp);
	g_free (str);
	g_free (str1);
        g_value_unset (&value);
}


static void
sysinfo_copy (GtkWidget *menuitem, gpointer *data)
{
	GtkTreeSelection *selection = NULL;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree));
	gtk_tree_selection_selected_foreach (selection,
					     (GtkTreeSelectionForeachFunc)get_selected_string,
					     GTK_TREE_VIEW(tree));
	if (selected_string) {
		gtk_clipboard_set_text(gtk_clipboard_get (GDK_SELECTION_CLIPBOARD),
				       selected_string,
				       strlen (selected_string));
		selected_string = NULL;
		g_free (selected_string);
	}
}

void
sysinfo_selected (GtkTreeSelection *treeselection,
		  gpointer user_data)
{
	if (!GTK_WIDGET_SENSITIVE (edit_menu[0].widget)) {
		gtk_widget_set_sensitive (GTK_WIDGET (edit_menu[0].widget), 						  TRUE);
	}
	if (!GTK_WIDGET_SENSITIVE (copy_menu_item)) {
		gtk_widget_set_sensitive (copy_menu_item, TRUE);
	}
}

static GtkWidget * 
do_popup_menu (GtkTreeView *view)
{
	GtkWidget *menu_item;
	GtkWidget *popup = NULL;
	GtkAccelGroup *accel_group;

	accel_group = gtk_accel_group_new ();

	popup = gtk_menu_new ();
        gtk_menu_set_accel_group (GTK_MENU (popup), accel_group);	
	menu_item = gtk_image_menu_item_new_from_stock (GTK_STOCK_SAVE_AS, 
						        accel_group);
	gtk_menu_shell_append (GTK_MENU_SHELL (popup), menu_item);
	g_signal_connect (menu_item, "activate",
			  G_CALLBACK (save_callback), NULL);
	gtk_widget_show (menu_item);

	copy_menu_item = gtk_image_menu_item_new_from_stock (GTK_STOCK_COPY, 
							accel_group);
	gtk_menu_shell_append (GTK_MENU_SHELL (popup), copy_menu_item);
	gtk_widget_set_sensitive (copy_menu_item, FALSE);
	g_signal_connect (copy_menu_item, "activate",
			  G_CALLBACK (sysinfo_copy), view);
	gtk_widget_show (copy_menu_item);

	return popup;
}

static gint 
tree_clicked (GtkWidget *tree, GdkEventButton *event, gpointer menu)
{
	if (event->type == GDK_BUTTON_PRESS) {
		if (event->button == 3) {
			/* don't change the List selection. */
			g_signal_stop_emission_by_name (tree, 
							"button_press_event");

			gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL,
		        		NULL, event->button, event->time);
		} else if (event->button == 1) {
			return FALSE;
		}
	}
	return TRUE;
}

static void
popup_position_func (GtkMenu   *menu,
	      gint      *x,
	      gint      *y,
	      gboolean  *push_in,
	      gpointer  user_data)
{
	GtkTreeView *tree_view = GTK_TREE_VIEW (user_data);
	GtkWidget *widget = GTK_WIDGET (tree_view);
	GtkRequisition req;

	g_return_if_fail (GTK_WIDGET_REALIZED (tree_view));

	gdk_window_get_origin (widget->window, x, y);

	gtk_widget_size_request (GTK_WIDGET (menu), &req);

	*x += widget->allocation.width / 2;
	*y += widget->allocation.height / 2;

	*x = CLAMP (*x, 0, MAX (0, gdk_screen_width () - req.width));
	*y = CLAMP (*y, 0, MAX (0, gdk_screen_height () - req.height));
}

static gint 
treeview_popup_menu (GtkWidget *tree_view, gpointer menu)
{
	GdkEventButton event;

	g_return_val_if_fail (tree_view != NULL && menu != NULL, FALSE);

	event.button = 3;
	event.time = GDK_CURRENT_TIME;
	gtk_menu_popup (GTK_MENU (menu), NULL, NULL,
				popup_position_func, tree_view,
				event.button, event.time);
	return TRUE;
}

/*
 * Update the system information displayed
 */
gboolean
sysinfo_update (gpointer data)
{
	GdkRectangle update_rect;
	gint width, height;

	update_virtual_memory ();
	update_tree (tree, descriptions[si_mem_swap], info[si_mem_swap]);
	update_tree (tree, descriptions[si_mem_inuse], info[si_mem_inuse]);

	get_uptime ();
	update_tree (tree, descriptions[si_uptime], info[si_uptime]);

	get_network_domain ();
	update_tree (tree, descriptions[si_ndomain], info[si_ndomain]);

	get_internet_domain ();
	update_tree (tree, descriptions[si_idomain], info[si_idomain]);

	update_rect.x = 0;
	update_rect.y = 0;
	update_rect.width = scroll_window->allocation.width;
	update_rect.height = scroll_window->allocation.height;
	gtk_widget_queue_draw_area (scroll_window, 
				    (&update_rect)->x, 
				    (&update_rect)->y, 
				    (&update_rect)->width, 
				    (&update_rect)->height);
	return TRUE;
}

static gint
save_session (GnomeClient        *client,
	      gint                phase,
	      GnomeRestartStyle   save_style,
	      gint                shutdown,
	      GnomeInteractStyle  interact_style,
	      gint                fast,
	      gpointer            client_data)
{
	gchar *argv[]= { NULL };

	argv[0] = (gchar*) client_data;
	gnome_client_set_clone_command (client, 1, argv);
	gnome_client_set_restart_command (client, 1, argv);

	return TRUE;
}

static gint
client_die (GnomeClient *client, gpointer client_data)
{
	gtk_main_quit ();
}

gint
main (gint argc, gchar **argv)
{
	GnomeClient *client;
	GtkWidget *menu;
	GtkTreeSelection *sel;
	gint width, height;

	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain(PACKAGE);

	gnome_program_init ("system-details", "1.0", LIBGNOMEUI_MODULE, 
			    argc, argv,GNOME_PARAM_APP_DATADIR,DATADIR,NULL);

	app = gnome_app_new("system-details", _("System Details"));	

	g_signal_connect(app, "delete_event",
			 G_CALLBACK (sysinfo_delete), NULL);
	gnome_app_create_menus (GNOME_APP (app), sysinfo_menu);
	gtk_widget_set_sensitive (GTK_WIDGET (edit_menu[0].widget), FALSE);


	tree = create_tree ();

	/* Get the system information */
	get_system_info ();

	scroll_window = gtk_scrolled_window_new (NULL, NULL);
	gtk_window_set_position (GTK_WINDOW (app), GTK_WIN_POS_CENTER);

	gtk_container_add (GTK_CONTAINER (scroll_window), GTK_WIDGET (tree));
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroll_window), 
					GTK_POLICY_AUTOMATIC, 
					GTK_POLICY_AUTOMATIC);
	gtk_widget_show (GTK_WIDGET (tree));

	/* Create the popup menu */
	menu = do_popup_menu (tree);
	gtk_widget_set_events (GTK_WIDGET (tree), GDK_BUTTON_PRESS_MASK);
	sel = gtk_tree_view_get_selection (tree);
	g_signal_connect (sel, "changed", G_CALLBACK (sysinfo_selected), NULL);
	g_signal_connect (tree, "button_press_event",
			  G_CALLBACK (tree_clicked), menu);
	g_signal_connect (tree, "popup_menu",
			  G_CALLBACK (treeview_popup_menu), menu);

	/* Fill the tree with strings to be displayed */
	fill_tree (tree, descriptions, info, &width, &height);

	gtk_widget_set_size_request (GTK_WIDGET (scroll_window), width, height);

	if ((client = gnome_master_client ()) != NULL) {
		g_signal_connect (client, "save_yourself",
				  G_CALLBACK (save_session), 
				  (gpointer) argv[0]);
		g_signal_connect (client, "die",
				  G_CALLBACK (client_die), NULL);
	}

	gnome_app_set_contents(GNOME_APP(app), scroll_window);

	gtk_widget_show_all (app);
	update = gtk_timeout_add (TIMEOUT, sysinfo_update, NULL);

	gtk_main ();

	return 0;
}
